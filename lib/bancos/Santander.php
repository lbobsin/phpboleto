<?php
/**
-----------------------
    COPYRIGHT
-----------------------
    Licensed under The MIT License.
    Redistributions of files must retain the above copyright notice.

    @author  Cláudio Medeiros <contato@claudiomedeiros.net>
    @package ObjectBoleto http://github.com/klawdyo/PHP-Object-Boleto
    @subpackage ObjectBoleto.Lib.Bancos
    @license http://www.opensource.org/licenses/mit-license.php The MIT License
    
-----------------------
    CHANGELOG
-----------------------
    17/05/2011
    [+] Inicial
    
    
    
  */
class Santander extends Banco{
    public $Codigo = '033';
    public $Nome = 'Santander';
    public $Css;
    public $Image = 'santander.png';

    /*
        @var Integer multiplicador
        Armazena o multiplicador a ser utilizado no calculo do Dígito verificador geral da linha digitável 
    */  
    private $multiplicador = 2;
    
    /*
        @var array $tamanhos
        Armazena os tamanhos dos campos na geração do código de barras
        e da linha digitável
    */    
    public $tamanhos = array(
        #Campos comuns a todos os bancos
        'Banco'             => 3,   //identificação do banco
        'Moeda'             => 1,   //Código da moeda: real=9
        'DV'                => 1,   //Dígito verificador geral da linha digitável
        'FatorVencimento'   => 4,   //Fator de vencimento (Dias passados desde 7/out/1997)
        'Valor'             => 10,  //Valor nominal do título
        #Campos variávies
        'Fixo'              => 1,   //Fixo 9
        'Cedente'           => 7,   //Agencia e conta
        'Zeros'             => 5,   //Preencher com 00000        
        'NossoNumero'       => 8,  //Nosso número
        'IOF'               => 1,  //Seguradoras valor entre 7 e 9(%), demais 0        
        'Carteira'          => 3
    );

    /*
          @var $carteiras
          Identifica as carteiras disponíveis para esse banco.

          - A primeira carteira sempre é a carteira padrão.
          - Se a carteira for um array, esse array deverá conter as diferenças
          nos tamanhos entre essa carteira e a carteira padrão. Se for uma
          string, será considerado que os tamanahos são os mesmos da carteira
          padrão, e o valor será o nome da carteira.
          - Só é necessário informar os campos cujos tamanhos sejam diferentes
          dos da carteira padrão.
       */
    public $carteiras = array(
      '101',
      '102',
      '201'
    );

    /*
        @var string $layoutCodigoBarras
        armazena o layout que será usado para gerar o código de barras desse banco.
        Cada variável é precedida por dois-pontos (:), que serão substituídas
        pelos seus respectivos valores
     */
    public $layoutCodigoBarras = ':Banco:Moeda:DV:FatorVencimento:Valor:Fixo:Cedente:Zeros:NossoNumero:IOF:Carteira';
    
    
    public function particularidade($object)
    {        
        $object->Data['Fixo']         = '9';
        $object->Data['Cedente']      = $object->Data['Agencia'] . $object->Data['Conta'];
        $object->Data['Zeros']        = '00000';
        $object->Data['IOF']          = '0';
        $object->Boleto->NossoNumero  = Math::Mod11($object->Boleto->NossoNumero, 0, 0, true);
        $object->Data['DV']           = $this->gerarDv($object);
    }

    /**
    *@param Object object - como os dados para calcular o digito verificador 
    *@return Integer - retorna o Dígito verificador geral da linha digitável
    *O cálculo do dígito verificador do código de barras, na posição "5" é o módulo
    *"11", de 2 a 9, utilizando o dígito 1 para os restos 0, 10 ou 1.
    *Para o cálculo, considerar as posições de 1 a 4 e de 6 a 44, iniciando pela posição 44 e
    *saltando a posição 5.
    *Dividir o total da soma por onze. Assim sendo, se o resto igual 10 o digito será = 1
    *(um), se o resto igual a 1 (um) ou 0 (zero) o digito será 0 (zero)
    *Qualquer “RESTO” diferente de “0, 1 ou 10”, subtrair o resto de 11 para obter o dígito.
    */
    private function gerarDv($object)
    { 
      $total = 0;
      $total += $this->calcDvPorPosicao($object->Data['Carteira']);

      $total += $this->calcDvPorPosicao($object->Data['IOF']);

      $total += $this->calcDvPorPosicao($object->Data['NossoNumero']);
      // não calcular
      //$total = $this->calcDvPorPosicao($object->Data['Zeros']);

      $total += $this->calcDvPorPosicao($object->Data['Cedente']);

      $total += $this->calcDvPorPosicao($object->Data['Fixo']);

      $total += $this->calcDvPorPosicao($object->Data['Valor']);

      $total += $this->calcDvPorPosicao($object->Data['FatorVencimento']);
      // não calcular
      //$calcDV = $this->calcDvPorPosicao($object->Data['Zeros'])$object->Data['DV'];

      $total += $this->calcDvPorPosicao($object->Data['Moeda']);

      $total += $this->calcDvPorPosicao($object->Data['Banco']);

      // Após toda a incrementação do total, multiplica o total por 10
      $totalMultiplicado10 = $total*10;
      // Faz o mod de 11
      $mod11 = $totalMultiplicado10 % 11;

      // utilizando o dígito 1 para os restos 0, 10 ou 1
      // demais resultados de mod, subtrair de 11
      if($mod11 == 10 || $mod11 == 1 || $mod11 == 0)
      {
        $dv = 1;
      }else{
        $dv = 11 - $mod11;
      }

      // retorna o digito
      return $dv;
    }


    /**
    *
    *@param String texto - texto com os números a serem calculados
    *@return Integer retorna o valor calculado para o campo, onde é invertido e multiplicado cada posição
    * pelo multiplicador(que sempre inicia com 2 e termina em 9, reiniciando sempre em 2)
    *Utilizar o módulo 11 – peso de 2 a 9 - para o cálculo deste DV.
    *Multiplicar da direita para a esquerda, de 2 até 9, até o final do número, reiniciando em
    *2 se necessário.
    */

    private function calcDvPorPosicao($texto)
    {
      $count = strlen($texto);
      $texto = strrev($texto);

      $total = 0;
      for ($i=1; $i <= $count ; $i++) 
      {
        $calculo = (int)$texto{$i-1} * $this->multiplicador;
        $total  += $calculo;
        $this->multiplicador++;

        // multiplicador deve ser sempre valores de 2 a 9, se for passar reinicia
        if($this->multiplicador == 10)
        {
          $this->multiplicador = 2;
        }
      }     

      return $total;
    }    
    
}