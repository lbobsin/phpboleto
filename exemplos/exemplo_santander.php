<?php
    header('Content-type: text/html; charset=utf-8');
    include '../OB_init.php';

    $ob = new OB('033');

    //*
    $ob->Vendedor

            ->setAgencia('1234')
            ->setConta('1234567')
            ->setCarteira('102')
            ->setRazaoSocial('José Claudio Medeiros de Lima')
            ->setCpf('012.345.678-39')
            ->setEndereco('Rua dos Mororós 111 Centro, São Paulo/SP CEP 12345-678')
            ->setEmail('joseclaudiomedeirosdelima@uol.com.br')
			->setCodigoCedente('7777777')
        ;

    //Define as configurações do boleto
    $ob->Configuracao
            ->setLocalPagamento('Pagável em qualquer banco até o vencimento')
            ->addInstrucao('Sr. Caixa. Não receber após 20 dias de atraso.')
            ->addInstrucao('Multa de 2% por atraso e 1% ao dia.')
            ->addDemonstrativo('Compra de produtos diversos.')
        ;
    
    $ob->Template
            ->setTitle('Gerador de Boleto')
            ->setTemplate('html5')
        ;

    $ob->Cliente
            ->setNome('Maria Joelma Bezerra de Medeiros')
            ->setCpf('111.999.888-39')
            ->setEmail('mariajoelma85@hotmail.com')
            ->setEndereco('')
            ->setCidade('')
            ->setUf('')
            ->setCep('')
        ;

    $ob->Boleto
            ->setValor(1100.00)
            //->setDiasVencimento(5)
            ->setVencimento(27,9,2016)
            ->setNossoNumero('87654')
            ->setNumDocumento('27.030195.10')
            ->setQuantidade(1)
        ;

    $ob->render(); 
   // $ob->plugin('Pdf')->save('/public/files/filename.pdf');
